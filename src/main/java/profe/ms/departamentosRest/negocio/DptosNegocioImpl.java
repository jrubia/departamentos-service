package profe.ms.departamentosRest.negocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import profe.empleados.model.Departamento;
import profe.empleados.model.Empleado;
import profe.ms.departamentosRest.daos.DptosDAO;
import profe.ms.empleadosweb.services.EmpleadosService;

@Service
public class DptosNegocioImpl implements DptosNegocio {
	
	@Autowired
	private DptosDAO dao;
		
	@Autowired
	private EmpleadosService empleadosService;

	@Override
	public List<Departamento> getAllDepartamentos() {
		// Recuperar Dptos del DAO;
		List<Departamento> dptos = dao.getAllDepartamentos();
		// Recupero todos empleados del servicio de empleados
		Map<String, Empleado> empleadosMap = 
				(HashMap<String, Empleado>) Arrays.stream(empleadosService.getAllEmpleados())
				.collect(Collectors.toMap(Empleado::getCif, Function.identity())); 
		// Para cada dpto
		dptos.forEach(dpto -> {
			// sustituyo su lista de empleados con la lista de empleados con datos completos
			List<Empleado> lNueva = new ArrayList<>();
			dpto.getEmpleados().forEach(empleado -> lNueva.add(empleadosMap.get(empleado.getCif())));
			dpto.setEmpleados(lNueva);
		});
		// Devuelvo la lista de dptos
		return dptos;
	}


}
